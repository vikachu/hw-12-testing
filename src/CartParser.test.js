import { readFileSync } from "fs";
import CartParser from "./CartParser";

let parser;

const getColumnNameKeys = () => ["name", "price", "quantity"];
const getErrorObjectKeys = () => ["type", "row", "column", "message"];

beforeEach(() => {
  parser = new CartParser();
});

describe("CartParser - unit tests", () => {
  // Add your unit tests here.

  describe("parse", () => {
    it("should throw Validation failed error when validation error occurs", () => {
      parser.readFile = jest.fn();
      parser.validate = jest.fn(() => [{}]);

      expect(() => {
        parser.parse("/path/");
      }).toThrow("Validation failed!");
    });

    it("should return object with keys: items and total when no error", () => {
      parser.readFile = jest.fn(() => "");
      parser.validate = jest.fn(() => []);
      parser.parseLine = jest.fn(() => {});
      parser.calcTotal = jest.fn(() => 0);

      const result = parser.parse("/path/");

      expect(Object.keys(result)).toEqual(
        expect.arrayContaining(["items", "total"])
      );
    });
  });

  describe("validate", () => {
    it("should return an empty array when no validation error", () => {
      const content = `Product name,Price,Quantity
      Mollis consequat,9.00,2`;

      expect(parser.validate(content)).toEqual([]);
    });

    it("should return Header type error when not expected column name meets", () => {
      const content = `Product WRONG name,Price,Quantity`;
      const expectedError = [
        {
          type: "header",
          row: 0,
          column: 0,
          message:
            'Expected header to be named "Product name" but received Product WRONG name.',
        },
      ];

      expect(parser.validate(content)).toEqual(expectedError);
    });

    it("should return Row type error when not accurate number of cells received", () => {
      const content = `Product name,Price,Quantity
      Mollis consequat,9.00`;
      const expectedError = [
        {
          type: "row",
          row: 1,
          column: -1,
          message: "Expected row to have 3 cells but received 2.",
        },
      ];

      expect(parser.validate(content)).toEqual(expectedError);
    });

    it("should return Cell type error when String cell is empty", () => {
      const content = `Product name,Price,Quantity
      ,9.00,2`;
      const expectedError = [
        {
          type: "cell",
          row: 1,
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".',
        },
      ];

      expect(parser.validate(content)).toEqual(expectedError);
    });

    it("should return Cell type error when Number cell is not positive number", () => {
      const content = `Product name,Price,Quantity
      Mollis consequat,9.00,-2`;
      const expectedError = [
        {
          type: "cell",
          row: 1,
          column: 2,
          message: 'Expected cell to be a positive number but received "-2".',
        },
      ];

      expect(parser.validate(content)).toEqual(expectedError);
    });

    it("should return an array of errors of length 2 when both Header and Cell errors occur", () => {
      const content = `Product name,Price,Quantity
      ,9.00,-2`;

      const errors = parser.validate(content);

      expect(errors.length).toBe(2);
    });
  });

  describe("parse line", () => {
    it("should add id field to JSON object when parse line", () => {
      const line = "Mollis consequat,9.00,2";

      const item = parser.parseLine(line);

      expect(item.hasOwnProperty("id")).toBe(true);
    });

    it("should return JSON object with column names as keys when parse line", () => {
      const line = "Mollis consequat,9.00,2";
      const columnNames = getColumnNameKeys();

      const item = parser.parseLine(line);

      expect(Object.keys(item).length).toBe(columnNames.length + 1);
      expect(Object.keys(item)).toEqual(expect.arrayContaining(columnNames));
    });
  });

  describe("create error", () => {
    it("should return error object with keys: type, row, column, message", () => {
      const errorKeys = getErrorObjectKeys();

      const error = parser.createError("header", 1, 2, "error message");

      expect(Object.keys(error)).toEqual(errorKeys);
    });
  });
});

describe("CartParser - integration test", () => {
  // Add your integration test here.
  it("should return correct total from parsed csv file", () => {
    const expectedResult = readFileSync(process.cwd() + "/samples/cart.json");
    const jsonExpectedResult = JSON.parse(expectedResult);

    const result = parser.parse(process.cwd() + "/samples/cart.csv");

    expect(result.total).toEqual(jsonExpectedResult.total);
  });
});
